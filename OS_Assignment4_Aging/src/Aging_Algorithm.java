import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;


public class Aging_Algorithm {

	public static void main(String args[])
	{
		//Get the file to read the page references from desktop
		File file = new File("F:\\SCU_First Quarter\\OS\\Assignments\\Page_Seq.txt");
					
		try {
			boolean continue_prog = true;
			//Create a hash map to store the page faults per 1000 memory locations and the frames
			Map<Integer,Integer> pageFaults = new HashMap<Integer,Integer>();
			
			while(continue_prog)
			{
			int page_faults = 0;
			Scanner file_read = new Scanner(file);
			Scanner c = new Scanner(System.in);

			//Get the number of frames from user
			System.out.println("Enter the number of frames");
			int frames = Integer.parseInt(c.nextLine());
			
			//Create a hash map to store the pages and their count in the frames
			Map<Integer,Integer> pageNums = new HashMap<Integer,Integer>(frames);		
			int turn = 0;
			int count = 0;
			
			while(file_read.hasNext())
			{
				turn = turn + 1;
				//Get the next page number from the file
				int pageNumber = Integer.parseInt(file_read.next());
				
				//Store the initial pages in the frames which are empty
				if(turn <= frames)
				{
					count = 1;
					
					//Check if the referenced page is already present in the frames
					if(pageNums.containsKey(pageNumber))
					{
						//Get the count of the present page
						count = pageNums.get(pageNumber);
						
						//Check if it has reached the maximum count limit
						if (count == 127)
						{
							count = 1;
						}		
						
						//Binary format of count and shifted to left
						String count_bin = (Integer.toBinaryString(count << 1));
						
						//Adding one to the count
						count = Integer.valueOf(count_bin, 2) + 1;
						turn = turn - 1;
					}
					//Insert the page and its count in the hashmap 
					pageNums.put(pageNumber,count);
				}
				else//frames fully occupied
				{
					//Check if the referenced page is already present
					if(pageNums.containsKey(pageNumber))
					{
						//Get the count of the number of references
						count = pageNums.get(pageNumber);
						
						//Check if maximum count limit is reached
						if (count == 127)
						{
							count = 1;
						}
						
						//Convert it into binary format and left shift
						String count_bin = (Integer.toBinaryString(count << 1));
						
						//Add one to the count
						count = Integer.valueOf(count_bin, 2) + 1;
						
						//Store the page number and the count 
						pageNums.put(pageNumber,count);
					}
					else //if new page number
					{
						//create an iterator object to traverse the hashmap
						Iterator<Entry<Integer, Integer>> itr = pageNums.entrySet().iterator();
						
						int pageRemove = 0;
						//Get the minimum count from the hashmap
						int min_count = Collections.min(pageNums.values());
						
						while(itr.hasNext())
						{
							//Get the page number having the minimum count
							Map.Entry<Integer,Integer> entry = itr.next();
						    if (entry.getValue().equals(min_count)) {
						        pageRemove = entry.getKey();
						    }
						}
				        
						//Remove the page with minimum count
						pageNums.remove(pageRemove);
						count = 1;
						
						//Insert the new page number with count = 1
						pageNums.put(pageNumber,count);

						//Increment the page fault
						page_faults = page_faults + 1;
					}
				}
			}	
			//Print the number of page faults
			System.out.println("Number of page faults are "+page_faults);
			int faults_1000 = page_faults/1000;
			System.out.println("Number of page faults per 1000 memeory reference are "+faults_1000);
			pageFaults.put(frames, faults_1000);
			
			System.out.println("Do you wish to enter more frames(YES or NO) ?");
			String ans = c.nextLine();
			
			if(ans.equals("NO"))
			{
				continue_prog = false;
				continue;
			}
			else if(ans.equals("YES"))
			{
				continue_prog = true;
				continue;
			}
			else
			{
				System.out.println("Enter valid input");
				continue;
			}
		}
			
			//Display the overall page fault report
			//create an iterator object to traverse the hashmap
			Iterator<Entry<Integer, Integer>> itr2 = pageFaults.entrySet().iterator();
			
			System.out.println("");
			System.out.println("Page Frames     Page Faults per 1000 memory references");
			while(itr2.hasNext())
			{
				//Get the page number having the minimum count
				Map.Entry<Integer,Integer> entry = itr2.next();
				System.out.println(entry.getKey() +"             "+ entry.getValue());
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
}
